<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="author" content="applebay.lt" />
    
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link rel="stylesheet" type="text/css" href="css/menu.css" />
    <link rel="stylesheet" type="text/css" href="css/blocks.css" />
    <link rel="stylesheet" type="text/css" href="css/frame.css" />
    
    
    
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/corner.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    
    <?php
    
    session_start();
    
    error_reporting(0);
    if(!$_SESSION['category']){
        $_SESSION['category']='home';
    }
    
    $category = $_SESSION['category'];

    if($category=='ipod' || $category=='iphone' || $category=='accessories' || $category =='register'){
        
        if($category=='ipod' || $category=='iphone' || $category=='accessories'){
            $link="goods/$category";
        }
        
        if($category=='register'){
            $link = "user/register.php";
        }
    }
    else{
        if($category == 'register'){
            
        }
        else{
            $link=$category;
        }
    }
    
       
    
    ?>
    
    
    <script type="text/javascript">
    
    $(document).ready(function(){
        
        jQuery.fx.off = false;
        
        frameload('<?php echo($link)?>');
        $("#page, .blockcontainer, #framecontainer, #popupcontainer").corner();
        
        
        //sponsors slideshow
        
        var sponsorscount = $(".sponsors").size();
        
        if(sponsorscount>1){
            window.setInterval(
                function(){
                    
                    $(".sponsors:first").fadeOut(500);
                    $(".sponsors:eq(1)").fadeIn(1000);
                    
                    setTimeout(function(){
                        $(".sponsors:first").insertAfter(".sponsors:last");
                    },
                    2000
                    )
            
            
                },5000
            )
      }
      
      
      
        
    })
    
    </script>
    
    
    
	<title>AppleBay</title>
</head>

<body>
    <center>
        <div id="headerbg">
            <div id="header">
                <div id="logo">
                    <div id="cart" onclick="frameload('cart')">krepselis</div>
                </div>
                
            </div>
        </div>
        <div id="container">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td id="leftside">
                    </td>
                    <td valign="top" id="page">
                        <div>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="11" id="menu-left">
                                    </td>
                                    <td height="44" id="menu-mid">
                                        
                                        <table id="menu" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td height="44" onclick="frameload('home')" width="100" align="center" valign="middle">
                                                    <div id="homebutton">
                                                    </div>
                                                </td>
                                                <td width="3" class="buttonline"></td>
                                                <td onclick="frameload('ipod')" id="ipod" class="menu-button">
                                                    iPod
                                                </td>
                                                <td width="3" class="buttonline">
                                                </td>
                                                <td onclick="frameload('iphone')" id="iphone" class="menu-button">
                                                    iPhone
                                                </td>
                                                <td width="3" class="buttonline">
                                                </td>
                                                <td onclick="frameload('accessories')" id="accessories" class="menu-button">
                                                    Aksesuarai
                                                </td>
                                                <td width="3" class="buttonline"></td>
                                                <td>
                                                    <div id="search">
                                                        <div id="searchicon">
                                                        </div>
                                                        <input size="5" value="search" type="text"/>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </td>
                                    <td width="11" id="menu-right">
                                    </td>
                                </tr>
                            </table>
                            
                            <table cellpadding="0" cellspacing="10">
                                <tr>
                                    <td id="framecontainer">
                                        <iframe scrolling="no" onload="resize()" id="frame" name="frame"></iframe>
                                    </td>
                                
                            
                                    <td valign="top">
                                    
                                        <div id="shareblock">
                                            <div class="blockcontainer">
                                                <div class="shares">
                                                    <img class="shareimages" src="imgs/shares/shares.png" />
                                                    <div class="blocktext">
                                                    Nuo �iol kiekvienas pirkinys svarbus. Isigykite Apple iPod/iPhone ir gaukite net iki 30% nuolaida jo priedams.
                                                    </div>
                                                    <div onclick="popupload()" class="sbutton"><div>placiau</div></div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <?php 
                                        
                                            if($_SESSION['status']=='logedin'){ 
                                            
                                                ?>
                                                
                                                <div class="block">
                                                    <div class="blocktitle">
                                                        <div class="blocktitletext">
                                                            <?php echo($_SESSION['email']);?>
                                                        </div>
                                                        <div class="blocktitlefog">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="blockcontainer">
                                                        <div class="sidebuttons" onclick="frameload('user')">
                                                            jusu uzsakimai / tvarkomi daiktai
                                                        </div>
                                                        
                                                        <hr size="1" color="#E5E5E5"/>
                                                        
                                                        <div onclick="frameload('user/cfg/usercfg.php')" class="sidebuttons">
                                                            duomenu keitimas
                                                        </div>
                                                        
                                                        
                                                        
                                                        <hr size="1" color="#E5E5E5"/>
                                                        
                                                        <form action="user/logout.php">
                                                            <div class="sbutton"><input type="submit" value="atsijunkti"/></div>
                                                        </form>
                                                    </div>
                                                </div>
                                            
                                            <?php
                                            
                                            }
                                            
                                            if($_SESSION['status']=='msd'){ 
                                            
                                                ?>
                                                
                                                <div class="block">
                                                    <div class="blocktitle">
                                                        <div class="blocktitletext">
                                                            Iveskite specialu koda
                                                        </div>
                                                        <div class="blocktitlefog">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="blockcontainer">
                                                        
                                                        <?php include('user/msd/check.php');?>
                                                        
                                                        
                                                        <hr size="1" color="#E5E5E5"/>
                                                        <form action="user/logout.php">
                                                            <div class="sbutton"><input type="submit" value="atsijunkti"/></div>
                                                        </form>
                                                    </div>
                                                </div>
                                            
                                            <?php
                                        }
                                        if($_SESSION['status']=='admin'){ 
                                            
                                                ?>
                                                
                                                <div class="block">
                                                    <div class="blocktitle">
                                                        <div class="blocktitletext">
                                                            Svetaines Redagavimas
                                                        </div>
                                                        <div class="blocktitlefog">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="blockcontainer">
                                                        <div class="sidebuttons" onclick="frameload('user/msd/msd/sponsors/sponsors.php')">
                                                            Remeju Redagavimas
                                                        </div>
                                                        <hr size="1" color="#E5E5E5"/>
                                                        <div class="sidebuttons" onclick="frameload('user/msd/msd/news/news.php')">
                                                            Naujienu Redagavimas
                                                        </div>
                                                        <hr size="1" color="#E5E5E5"/>
                                                        <div class="sidebuttons" onclick="frameload('user/msd/msd/requests/requests.php')">
                                                            Uzsakimu Redagavimas
                                                        </div>
                                                        <hr size="1" color="#E5E5E5"/>
                                                        <div class="sidebuttons" onclick="frameload('user/msd/msd/slidemenu/slidemenu.php')">
                                                            Praeinancio meniu Redagavimas
                                                        </div>
                                                        <hr size="1" color="#E5E5E5"/>
                                                        <div class="sidebuttons" onclick="frameload('user/msd/msd/goods/goods.php')">
                                                            Prekiu Redagavimas
                                                        </div>
                                                        <hr size="1" color="#E5E5E5"/>
                                                        
                                                        <form action="user/logout.php">
                                                            <div class="sbutton"><input type="submit" value="atsijunkti"/></div>
                                                        </form>
                                                    </div>
                                                </div>
                                            
                                            <?php
                                            
                                            }  
                                            
                                        ?>
                                        
                                        
                                        <?php if(!$_SESSION['status']){?>
                                        <div class="block">
                                            <div class="blocktitle">
                                                <div class="blocktitletext">
                                                    Vartotojo prisijungimas
                                                </div>
                                                <div class="blocktitlefog">
                                                </div>
                                            </div>
                                            <div class="blockcontainer">
                                                <div id="logininputs">
                                                    <form action="user/login.php" method="POST">
                                                        <div class="inputtitle">Elektroninis pastas:</div>
                                                        <div class="input">
                                                            <input id="email" name="email" value="etas@etauskas.lt" />
                                                        </div>
                                                        <div class="inputtitle">Slaptazodis:</div>
                                                        <div class="input">
                                                            <input id="password" name="password" type="password" value="2222" />
                                                        </div>
                                                            
                                                            <div class="sbutton"><input type="submit" value="prisijunkti"/></div>
                                                    </form>
                                                </div>
                                                <div id="loginhotlinks">
                                                    <div class="hotlinks" onclick="frameload('user/register.php')">Registracija</div>
                                                    <div class="hotlinks" onclick="frameload('http://www.uzdarbis.lt')">Pamirsote slaptazodi?</div>
                                                </div>
                                                
                                                
                                                
                                            </div>
                                        </div>
                                        <?php } ?>
                                        
                                        <div class="block" id="sponsorsblock">
                                            <div class="blocktitle">
                                                <div class="blocktitletext">
                                                    Mus remia
                                                </div>
                                                <div class="blocktitlefog">
                                                </div>
                                            </div>
                                            <div class="blockcontainer">
                                                <div id="sponsorscontainer">
                                                    <?php
                                                         include('join/join.php');
                                                         
                                                         $querysponsors = mysql_query("SELECT img FROM sponsors");
                                                         
                                                         while($rowsponsors = mysql_fetch_array($querysponsors)){
                                                            $sponsorsimgname = $rowsponsors['img'];
                                                    ?>
                                                         <img class="sponsors" src="imgs/sponsors/<?php echo($sponsorsimgname)?>"/>
                                                    <?php
                                                         }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="block">
                                            <div class="blocktitle">
                                                <div class="blocktitletext">
                                                    Klientu aptarnavimas
                                                </div>
                                                <div class="blocktitlefog">
                                                </div>
                                            </div>
                                            <div class="blockcontainer">
                                                
                                                <div id="userhotlinks">
                                                    
                                                    <img class="hotlinkicon" src="imgs/web/page/container/apple.png" height="20px"/>
                                                    <div onclick="popupload()" class="bbutton"><div>Pristatymas</div></div>
                                                    
                                                    <img class="hotlinkicon" src="imgs/web/page/container/apple.png" height="20px"/>
                                                    <div onclick="popupload()" class="bbutton"><div>Kaip Uzsiskayti</div></div>
                                                    
                                                    <img class="hotlinkicon" src="imgs/web/page/container/apple.png" height="20px"/>
                                                    <div onclick="popupload()" class="bbutton"><div>Atsiskaitimo Budai</div></div>
                                                    
                                                    <img class="hotlinkicon" src="imgs/web/page/container/apple.png" height="20px"/>    
                                                    <div onclick="popupload()" class="bbutton"><div>DUK / Informacija</div></div>
                                                    
                                                    <img class="hotlinkicon" src="imgs/web/page/container/apple.png" height="20px"/>
                                                    <div onclick="popupload()" class="bbutton"><div>Garantija</div></div>
                                                    
                                                    <img class="hotlinkicon" src="imgs/web/page/container/apple.png" height="20px"/>
                                                    <div onclick="frameload('news')" class="bbutton"><div>Naujienos / Akcijos</div></div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td id="rightside">
                    </td>
                </tr>
            </table>
        </div>
            
        <div id="footer">
            <hr style="margin: 5px;" size="1" color="#E5E5E5"/>
            <table align="center">
                <tr>
                    <td>
                        <div class="hotlinks">pradinis</div>
                        
                    </td>
                    <td class="vline">
                        |
                    </td>
                    <td>
                        <div class="hotlinks">iPod</div>
                    </td>
                    <td class="vline">
                        |
                    </td>
                    <td>
                        <div class="hotlinks">iPhone</div>
                    </td>
                    <td class="vline">
                        |
                    </td>
                    <td>
                        <div class="hotlinks">Aksesuarai</div>
                    </td>
                    <td class="vline">
                        |
                    </td>
                    <td>
                        <div class="hotlinks">Servisas</div>
                    </td>
                    <td class="vline">
                        |
                    </td>
                    <td>
                        <div class="hotlinks">Susisiekti</div>
                    </td>
                    
                    <td rowspan="2">
                        <div id="help">
                            <div id="skype">
                            </div>
                            <div id="number">
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td align="center" colspan="11">
                        <div id="copyrights">
                            Visos teises saugoms. � 20010�2011. Design by AppleBay
                        </div>
                    </td>
                </tr>
        </table>
    </div>
    </center>
    
    
    
    <div id="fog" onclick="popupclose()">
    </div>
    <div id="popup">
        <div id="popupclose" onclick="popupclose()"></div>
        <div id="popupcontainer"></div>
    </div>
</body>
</html>