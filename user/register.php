<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="author" content="applebay.lt" />

    <link charset="text/css" rel="stylesheet" href="../css/register.css" />
    
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/corner.js"></script>
    
</head>
<script type="text/javascript">
$(document).ready(function(){
       
       $("#advantiges").corner("right");
       $("#regtitle").corner("left");

})
</script>
<body>

<?php

session_start();

$_SESSION['category']='register';

include('../join/join.php');



?>
<center>

<div id="regtitle">
    <div>
        <label>Registracija</label>
    </div>
</div>

<table id="register" cellpadding="0" cellspacing="0" width="580">
                
    
    


    <form method="POST" action="registersubmit.php">
        
        <tr>
            
            <td height="10">
            </td>
        </tr>
        
        <tr>
            <td>
            </td>
            <td class="line" colspan="3">
            </td>
        </tr>
        <tr>
            <td height="10">
            </td>
        </tr>
        
        
        <tr>
            <td class="inputtitle">
                Elektroninis pastas
            </td>
            <td>
                <div class="inputbg">
                    <input class="input" type="text" name="email" />
                </div>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="pvz">
                pvz: Email@email.com
            </td>
        </tr>
        <tr>
            <td class="inputtitle">
                Slaptazodis 
            </td>
            <td>
                <div class="inputbg">
                    <input class="input" name="password" type="password"/>
                </div>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="pvz">
                Gali buti tik skaiciai ir raides
            </td>
        </tr>
        <tr>
            <td class="inputtitle">
                Pakartokite slaptazodi 
            </td>
            <td>
                <div class="inputbg">
                    <input class="input" name="repeatpassword" type="password"/>
                </div>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="pvz">
                Slaptazodis ir pakartotas slaptazodis turi sutapti
            </td>
        </tr>
        
        <tr>
            
            <td height="15">
            </td>
        </tr>

<tr>
    <td class="inputtitle">
        Vardas 
    </td>
    <td>
        <div class="inputbg">
            <input class="input" name="name" type="text"/>
        </div>
    </td>
    <td>
    </td>
</tr>
        <tr>
            <td colspan="2" class="pvz">
            </td>
        </tr>
<tr>
    <td class="inputtitle">
        Pavarde 
    </td>
    <td>
        <div class="inputbg">
            <input class="input" name="lastname" type="text"/>
        </div>
    </td>
    <td>
    </td>
</tr>
        <tr>
            <td colspan="2" class="pvz">
            </td>
        </tr>

<tr>
    <td class="inputtitle">
        Miestas 
    </td>
    <td>
        <div class="inputbg">
            <input class="input" name="town" type="text"/>
        </div>
    </td>
    <td>
    </td>
</tr>
        <tr>
            <td colspan="2" class="pvz">
            </td>
        </tr>
<tr>
    <td class="inputtitle">
        Adresas 
    </td>
    <td>
        <div class="inputbg">
            <input class="input" name="adress" type="text"/>
        </div>
    </td>
    <td>
    </td>
</tr>
        <tr>
            <td colspan="2" class="pvz">
                pvz: Duseikiu 12-14
            </td>
        </tr>
<tr>
    <td class="inputtitle">
        Telefono numeris 
    </td>
    <td>
        <div class="inputbg">
            <input class="input" name="phonenr" type="text"/>
        </div>
    </td>
    <td>
    </td>
</tr>
        <tr>
            <td colspan="2" class="pvz">
                pvz: +37061234567
            </td>
        </tr>
    <tr>
            
            <td height="10">
            </td>
        </tr>
        
        <tr>
            <td>
            </td>
            <td class="line" colspan="3">
            </td>
        </tr>
        <tr>
            <td height="10">
            </td>
        </tr>
<tr>

    <td>
    </td>
    <td align="right">
        <input id="submit" type="submit" value="Patvirtinti"/>
    </td>
</tr>
<tr>
        <td height="20">
        </td>
</tr>
</form>
</table>
    
    <div id="advantiges">
            <div id="advantigestytle">
                Tik prisiregistravusiems vartotojams
            </div>
            <ul id="advantigestext">
                <li>
                    Leidziama pirkit musu pardotuveje
                </li>
                <li>
                    Galite stebeti jusu uzsakytu prekiu pristatimo stadija
                </li>
                <li>
                    Galite stebeti jusu tvarkomu apple produktu stadija
                </li>
            </ul>
    </div>



</center>
</body>
</html>