/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50139
Source Host           : localhost:3306
Source Database       : applebay

Target Server Type    : MYSQL
Target Server Version : 50139
File Encoding         : 65001

Date: 2010-12-21 12:36:40
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `goods`
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `itemid` int(100) NOT NULL DEFAULT '0',
  `name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `cost` varchar(100) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `sharecost` varchar(100) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `about` text COLLATE utf8_lithuanian_ci,
  `count` int(100) DEFAULT NULL,
  `warranty` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `img` varchar(400) CHARACTER SET utf8 DEFAULT NULL,
  `bigabout` text CHARACTER SET utf8,
  `specifications` varchar(1500) CHARACTER SET utf8 DEFAULT NULL,
  `subcategorys` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(500) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `maincategorys` varchar(200) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('1', 'iPhone 3GS 8GB', '2099', '1500', 'Juodas iPhone 3GS 8GB\r\nVeikia su visų operatorių kortelėmis\r\n2.099,00 Lt\r\nJuodas iPhone 3GS 8GB\r\nVeikia su visų operatorių kortelėmis', '12', '', 'iPhone_3GS_8GB_no_1.jpg', '', '', '3gs', null, 'iPhone');
INSERT INTO `goods` VALUES ('2', 'iPod shuffle 2GB 4-osios kartos', '179', '100', 'Mažas, spalvotas ir vėl su valdymo ratuku!', '12', '', 'ipod_shuffle_4g_silver1_z3.jpg,ipod_shuffle_4g_silver2_z2.jpg,ipod_shuffle_4g_silver3_z1.jpg,ipod_shuffle_4g_silver4_z4.jpg', '', '', 'shuffle', null, 'iPod');
INSERT INTO `goods` VALUES ('3', 'iPod nano 8GB 6-osios kartos', '559', '300', 'Visiškai atnaujintas, su lietimui jautriu ekranu! Rožinės spalvos', '12', '', 'ipod_nano_6g_pink_1_z1.jpg,ipod_nano_6g_pink_2_z2.jpg,ipod_nano_6g_pink_3_z3.jpg', '', '', 'nano', null, 'iPod');
INSERT INTO `goods` VALUES ('4', 'iPod touch 8GB 4-osios kartos', '759', '500', 'Nuostabus Retina ekranas ir beribės galimybės!', '12', '', 'ipod_touch_4gen_1_z1.jpg,ipod_touch_4gen_2_z2.jpg,ipod_touch_4gen_3_z3.jpg,ipod_touch_4gen_4_z4.jpg', '', '', 'touch', null, 'iPod');
INSERT INTO `goods` VALUES ('5', 'iPod classic 160GB', '859', '700', '160GB vietos Vos už 859litus', '12', '', 'classic_2009_silver_1_z1.jpg,classic_2009_silver_3_z3.jpg,classic_2009_silver_4_z4.jpg', '', '', 'classic', null, 'iPod');
INSERT INTO `goods` VALUES ('6', 'Apple iPhone 4 16GB', '2999', '2000', 'Pažangus išmanusis telefonas su įspūdingu ekranu!', '5', null, 'iphone_4_1_z1.jpg,iphone_4_3_z2.jpg,iphone_4_4_z3.jpg,iphone_4_5_z4.jpg', null, null, '4gs', null, 'iPhone');
INSERT INTO `goods` VALUES ('7', 'iPod nano 16GB 6-osios kartos', '670.16', '54', 'Visiškai atnaujintas, su lietimui jautriu ekranu!\r\nPilkos spalvos', '4', null, 'ipod_nano_6g_graphite_1_z1.jpg,ipod_nano_6g_graphite_2_z2.jpg,ipod_nano_6g_graphite_3_z3.jpg,ipod_nano_6g_graphite_4_z4.jpg', null, null, 'nano', null, 'iPod');

-- ----------------------------
-- Table structure for `maincategorys`
-- ----------------------------
DROP TABLE IF EXISTS `maincategorys`;
CREATE TABLE `maincategorys` (
  `id` int(10) NOT NULL,
  `title` varchar(200) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `img` varchar(400) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `about` text COLLATE utf8_lithuanian_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of maincategorys
-- ----------------------------
INSERT INTO `maincategorys` VALUES ('1', 'iPod', 'ipods.jpg', 'vieni i� populiariausiu ne�iojamu muzikos grotuvu pasaulyje. I� ju tikrai i�sirinksite Jums tinkamiausia: ma�a ir mobilu iPod shuffle, nuo �iol su lietimui jautriu ekranu iPod nano, ypatingai dideles talpos iPod classic arba iPod touch su dideliu, net 3,5 coliu skersmens bei ypatingai rai�kiu ekranu.');
INSERT INTO `maincategorys` VALUES ('2', 'iPhone', 'iphones.jpg', 'vieni i� populiariausiu ne�iojamu muzikos grotuvu pasaulyje. I� ju tikrai i�sirinksite Jums tinkamiausia: ma�a ir mobilu iPod shuffle, nuo �iol su lietimui jautriu ekranu iPod nano, ypatingai dideles talpos iPod classic arba iPod touch su dideliu, net 3,5 coliu skersmens bei ypatingai rai�kiu ekranu.');
INSERT INTO `maincategorys` VALUES ('3', 'macukstis', 'Winter.jpg', 'medienas');

-- ----------------------------
-- Table structure for `members`
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `accid` int(10) NOT NULL DEFAULT '0',
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `lastname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `town` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `adress` varchar(400) CHARACTER SET utf8 DEFAULT NULL,
  `phonenr` varchar(100) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `status` varchar(100) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  PRIMARY KEY (`accid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('1', '', '', 'msd@gmail.com', '', '', null, '1234', 'msd');
INSERT INTO `members` VALUES ('2', 'Jon', 'keil', 'kozas@tenas.lt', 'Telšiai', 'Bangu 6', '+37065456465', '2222', 'user');
INSERT INTO `members` VALUES ('3', 'Petras', 'Petrauskas', 'petras@gmail.com', 'Palanga', 'Palangos 46', '+37046987264', '7897', 'user');
INSERT INTO `members` VALUES ('4', 'Egidijus', 'Talk', 'sue@gmail.com', 'Telsiai', 'Plento 12', '3706363633', '1234', 'user');

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `newsid` int(10) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `title` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `text` longtext COLLATE utf8_lithuanian_ci,
  `teaser` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `img` varchar(200) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  PRIMARY KEY (`newsid`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1', '2010-09-15', 'Atnaujinti iPod', 'Pagrindiniai atnaujinimai:\r\niPod touch grotuvas nuo šiol su Retina ekranu bei HD filmavimo galimybe.\r\niPod touch su daugiau atminties, greitesniu procesoriumi. Taigi dar tinkamesnis žaidimams bei kitoms programoms.\r\niPod nano grotuvai su lietimui jautriu ekranu. Mažesni už prieš tai buvusius modelius.\r\niPod nano su FM radijo imtuvu. \r\niPod shuffle vėl su mygtukais!\r\nPlanuojama, jog naujieji iPod prekyboje atsiras nuo spalio mėnesio.', 'Pagrindiniai atnaujinimai:\r\niPod touch grotuvas nuo šiol su Retina ekranu bei HD filmavimo galimybe.\r\niPod touch su daugiau atminties...', 'iPod_family.jpg');
INSERT INTO `news` VALUES ('3', '2010-07-30', 'Naujieji iMac', 'Apple kompanija atnaujino savo „viskas viename“ stalinių kompiuterių gamą.\r\n \r\nPagrindiniai atnaujinimai:\r\nNuo šiol kompiuteriuose rasite i3, i5 bei i7 architektūrų Intel procesorius.\r\nVisuose modeliuose yra atnaujintos grafikos plokštės. 27\" modeliuose jos bus net su iki 1GB atminties. Ir nuo šiol bus montuojami tik ATI gaminiai.\r\nPadidinta operatyviosios atminties magistralė, kuri nuo šiol bus 1333MHz (buvo 1066MHz).\r\nNumatoma naujųjų iMac prekybos data Lietuvoje kol kas neskelbiama. Bet ilgai laukti tikrai neteks. Plačiau skaitykite Apple puslapyje anglų kalba.\r\n \r\nKitos Apple naujienos:\r\nApple atnaujino LED Cinema Display vaizduoklį, kuris nuo šiol bus 27\". Taigi turėsite dar daugiau vietos ant įspūdingos kokybės ekrano. Tiesa šia preke kompanija numato pradėti prekiauti tik rugsėjį. Plačiau skaitykite Apple puslapyje anglų kalba.\r\nBuvo pristatytas pelės valdymo aikštelės imitatorius stalo kompiuteriams. Naujas gaminys, pavadinimu „Magic Trackpad“ Lietuvoje prekyboje pasirodys rugojūčio mėnesį. \r\nBuvo atnaujinti ir Mac Pro kompiuteriai, kurių pagrindinis atnaujinimas yra net iki 12 branduolių turintys procesoriai.', 'Apple kompanija atnaujino savo „viskas viename“ stalinių kompiuterių gamą.\r\n \r\nPagrindiniai atnaujinimai:\r\nNuo šiol kompiuteriuose...', 'overview_hero.jpg');
INSERT INTO `news` VALUES ('4', '2010-05-21', 'Atnaujinti MacBook', 'acBook pagrindiniai atnaujinimai:\r\nGalingesnis procesorius: 2,4Ghz Intel Core 2 Duo\r\nGalingesnė vaizdo posistemė: NVIDIA GeForce 320M 256MB\r\nNaujasis Apple MacBook prekyboje taip pat turi pasirodytibirželio pradžioje, o numatoma kaina -  3599 Lt', 'MacBook pagrindiniai atnaujinimai:\r\nGalingesnis procesorius...', 'overview_hero1_20091020.jpg');
INSERT INTO `news` VALUES ('5', '2010-05-21', 'Keičiasi kainos', 'Pasiekitė MacBook Pro kainos. Jos šiek tiek sumažėjo.\r\nMC374	MacBook Pro 13” Core 2 Duo 2.4GHz, 4GB, 250GB, GeForce 320M - 4299 3999Lt\r\nMC375	MacBook Pro 13” Core 2 Duo 2.66GHz, 4GB, 320GB, GeForce 320M- 5599 5299Lt\r\nMC371	 MacBook Pro 15” Core i5 2.4GHz, 4GB, 320GB, HD Graphics, GeForce GT 330M 256MB - 6799 6299 Lt\r\nMC372	MacBook Pro 15” Core i5 2.53GHz, 4GB, 500GB, HD Graphics, GeForce GT 330M 256MB - 7299 6799 Lt\r\nMC373 MacBook Pro 15” Core i7 2.66GHz, 4GB, 500GB, HD Graphics, GeForce GT 330M 512MB - 8099 7599 Lt\r\nMC02- MacBook Pro 17” Core i5 2.53GHz, 4GB, 500GB, HD Graphics, GeForce GT 330M 512MB - 8299 7799 Lt', 'Pasiekitė MacBook Pro kainos. Jos šiek tiek sumažėjo.\r\nMC374	MacBook Pro 13” Core 2 Duo 2.4GHz...', 'macbook_pro_mc371_overview_1.jpg');

-- ----------------------------
-- Table structure for `repair`
-- ----------------------------
DROP TABLE IF EXISTS `repair`;
CREATE TABLE `repair` (
  `repairid` varchar(100) COLLATE utf8_lithuanian_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`repairid`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of repair
-- ----------------------------
INSERT INTO `repair` VALUES ('10', 'asfgfdfg', 'etas@etauskas.lt');
INSERT INTO `repair` VALUES ('56', 'Tvarkomas', 'etas@etauskas.lt');
INSERT INTO `repair` VALUES ('82', 'asdfghhfgfh', 'etas@etauskas.lt');

-- ----------------------------
-- Table structure for `requests`
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests` (
  `reqid` int(150) NOT NULL DEFAULT '0',
  `email` varchar(100) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '',
  `date` date DEFAULT NULL,
  `items` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `allcost` varchar(1000) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `status` varchar(200) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  PRIMARY KEY (`reqid`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of requests
-- ----------------------------
INSERT INTO `requests` VALUES ('1010', 'etas@etauskas.lt', '2010-12-03', '6 1,2 1,3 1', '3737', 'Uzsakytas');
INSERT INTO `requests` VALUES ('1017', 'etas@etauskas.lt', '2010-12-06', '1 1', '2099', 'Uzsakytas');
INSERT INTO `requests` VALUES ('1018', 'etas@etauskas.lt', '2010-12-07', '6 1,3 1', '3558', 'Uzsakytas');
INSERT INTO `requests` VALUES ('1019', 'petras@gmail.com', '2010-12-07', '1 1,6 2,3 1', '8656', 'Ruosiamas');
INSERT INTO `requests` VALUES ('1020', 'msd@gmail.com', '2010-12-08', '1 1', '2099', 'Issiustas');
INSERT INTO `requests` VALUES ('1021', 'msd@gmail.com', '2010-12-08', '6 1,1 1', '5098', 'Uzsakytas');
INSERT INTO `requests` VALUES ('1022', 'msd@gmail.com', '2010-12-08', '3 1', '559', 'Uzsakytas');
INSERT INTO `requests` VALUES ('1023', 'msd@gmail.com', '2010-12-08', '4 1,6 1,1 1', '5857', 'Uzsakytas');

-- ----------------------------
-- Table structure for `shares`
-- ----------------------------
DROP TABLE IF EXISTS `shares`;
CREATE TABLE `shares` (
  `img` varchar(500) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `title` varchar(400) CHARACTER SET utf8 DEFAULT NULL,
  `text` text CHARACTER SET utf8,
  `link` varchar(500) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `shareid` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shareid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of shares
-- ----------------------------

-- ----------------------------
-- Table structure for `slidemenu`
-- ----------------------------
DROP TABLE IF EXISTS `slidemenu`;
CREATE TABLE `slidemenu` (
  `id` int(10) NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `img` varchar(300) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `url` varchar(400) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of slidemenu
-- ----------------------------
INSERT INTO `slidemenu` VALUES ('0', '', 'sveikiname.png', null);
INSERT INTO `slidemenu` VALUES ('1', 'Diagnostika', 'services.jpg', null);
INSERT INTO `slidemenu` VALUES ('2', 'Papildomos paslaugos', 'paslaugos.jpg', null);
INSERT INTO `slidemenu` VALUES ('3', 'Karsciausi pasiulymai', 'lastsugestion.jpg', null);
INSERT INTO `slidemenu` VALUES ('4', 'Uzsakymo bukle', 'bukle.jpg', null);

-- ----------------------------
-- Table structure for `sponsors`
-- ----------------------------
DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE `sponsors` (
  `sponsorid` int(255) NOT NULL,
  `img` varchar(600) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `about` text COLLATE utf8_lithuanian_ci,
  PRIMARY KEY (`sponsorid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of sponsors
-- ----------------------------
INSERT INTO `sponsors` VALUES ('1', 'dolita.jpg', 'Kedai kurie soka valsa');
INSERT INTO `sponsors` VALUES ('2', 'stilingabuk.jpg', ' pisciau smarke');

-- ----------------------------
-- Table structure for `subcategorys`
-- ----------------------------
DROP TABLE IF EXISTS `subcategorys`;
CREATE TABLE `subcategorys` (
  `id` int(10) NOT NULL,
  `title` varchar(200) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `img` varchar(400) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `about` text COLLATE utf8_lithuanian_ci,
  `maincategorys` varchar(220) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- ----------------------------
-- Records of subcategorys
-- ----------------------------
INSERT INTO `subcategorys` VALUES ('1', 'classic', 'classic.jpg', 'classic', 'iPod');
INSERT INTO `subcategorys` VALUES ('2', 'nanosss', 'nano.jpg', 'nano', 'iPod');
INSERT INTO `subcategorys` VALUES ('3', 'shuffle', 'shuffle.jpg', 'shuffle', 'iPod');
INSERT INTO `subcategorys` VALUES ('4', 'touch', 'touch.jpg', 'touch', 'iPod');
INSERT INTO `subcategorys` VALUES ('5', '3gs', '3gs.jpg', '3gs', 'iPhone');
INSERT INTO `subcategorys` VALUES ('6', '4gs', '4gs.jpg', '4gs', 'iPhone');
