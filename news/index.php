<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<meta name="author" content="applebay" />
    
    <link rel="stylesheet" charset="text/css" href="../css/home.css" />
    <link rel="stylesheet" charset="text/css" href="../css/news.css" />
    
    
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/corner.js"></script>
    <script type="text/javascript" src="../js/news.js"></script>
    
    <script>
    
    $(document).ready(function(){
        
        $("#snews, .conttitle, #newsinfo, .news, #snews").corner();
        
    })
    
    </script>
    
    <?php
    
    include('../join/join.php');
    session_start();
    
    $_SESSION['category']="news";
    
    ?>
    
	<title>news</title>
</head>

<body>
    <table width="600" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
                <div id="newsinfo">
                    <div class="conttitle">
                    </div>
                    
                    <div id="newscontainer">
                        <img src="../imgs/news/news.png" height="265px" />
                    </div>
                    
                </div>
            </td>
        </tr>
        <tr>
            <td height="10">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="snews">
                    <div class="conttitle">
                        <?php
                            $titlequery = mysql_query("SELECT title FROM news ORDER BY date DESC");
                            while($titlerows = mysql_fetch_array($titlequery)){
                                
                                ?>
                                    <div class='slidemenubuttons'>
                                        <b>
                                            <?php
                                                echo($titlerows['title']);
                                            ?>
                                        </b>
                                    </div>
                                <?php
                            }
                        ?>
                    </div>
                    <div id="snewscont">
                    </div>
                </div>
            </td>
        </tr>
        <tr>
        <?php $querynews = mysql_query("SELECT * FROM news ORDER BY date DESC"); 
                $i=1;
                $newscount = mysql_num_rows($querynews);
                    while($newsrows=mysql_fetch_array($querynews)){
                        
                        
                        if($i%2){
                            echo("</tr><tr>");
                        }
                        else{
                            echo('<td width="10"></td>');
                        }
                        if($i==5){
                            break;
                        }
        ?>
        
            <td valign="middle">
                <div class="news">
                    <div class="newscontainer">
                        <div class="newsdate"><?php echo($newsrows['date']) ?></div>
                        <div class="newstitle"><?php echo($newsrows['title']) ?></div>
                        <img class="newsimg" src="../imgs/news/<?php echo($newsrows['img']) ?>" height="120" />
                        <div class="newsteaser">
                            <?php echo($newsrows['teaser']) ?>
                        </div>
                        <div class="newsbutton">
                            <b>placiau</b>
                        </div>
                    </div>
                </div>
            </td>
                    <?php
                        
                        $i++;
                } ?>
        </tr>
    </table>
</body>

</html>